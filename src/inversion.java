public class inversion {
    public static void main(String[] args) {

        double[] array = {1.2, 5.5, 8.8, 4.2, 6.1, 7.8, 2.6, 3.4};

        System.out.print("[" + array[0]);

        for (int i = 1; i < array.length; i++) {
            System.out.print("," + array[i]);
        }
        System.out.println("]");

        for (int i = 0; i < array.length / 2; i++) {
            double temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
        System.out.print("[" + array[0]);
        for (int i = 1; i < array.length; i++) {
            System.out.print("," + array[i]);
        }
        System.out.print("]");
    }
}
