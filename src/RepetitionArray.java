public class RepetitionArray {
    public static void main(String[] args) {
        int[] array = {4, 5, 3, 2, 6, 8, 9, 6, 1, 0};
        boolean[] repeated = new boolean[10];
        for (int i = 0; i < array.length; i++) {
            int count = 1;
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j] && !repeated[j]) {
                    count++;
                    repeated[j] = true;
                }
            }
            if (count > 1) {
                System.out.println(array[i] + " - " + count);
            }
        }

    }
}
